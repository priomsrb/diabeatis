package {
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	public class TimeOfDayOverlay extends Entity {
		private var gameState:GameState;
		private var onAnimationEnd:Function;
		private var dayText:Text;
		
		private var sunImage: Image;
		private var blackRectangle: Canvas;
		
		public static const MORNING:int = 0;
		public static const MORNING_TO_AFTERNOON:int = 1;
		public static const AFTERNOON:int = 2;
		public static const AFTERNOON_TO_NIGHT:int = 3;
		public static const NIGHT:int = 4;
		public static const NIGHT_TO_MORNING:int = 5;
		
		public var timeOfDay:int = MORNING;
		
		public function TimeOfDayOverlay(gameState:GameState, onAnimationEnd:Function) {
			this.gameState = gameState;
			this.onAnimationEnd = onAnimationEnd;
		}
		
		override public function added():void {
			this.graphic = new Graphiclist;
			
			sunImage = new Image(Resources.DEFAULT_SUN);
			sunImage.x = -75;
			sunImage.y = -75;
			addGraphic(sunImage);
			
			blackRectangle = new Canvas(800, 600);
			blackRectangle.x = 0;
			blackRectangle.y = 0;
			blackRectangle.drawRect(new Rectangle(0, 0, 800, 600), 0x000000);
			blackRectangle.alpha = 0;
			addGraphic(blackRectangle);
			
									
			Text.size = 48;
			dayText = new Text("Day 000", 0, 150); // The text should be wide enough for at least 999 days
			dayText.x = 320;
			dayText.color = 0xFFFFFF;
			dayText.alpha = 0;
			addGraphic(dayText);
			
			if (gameState.turnsPlayed % 2 == 0) {
				timeOfDay = MORNING;
			} else {
				timeOfDay = AFTERNOON;
			}
			

			refresh();
		}
		

		public function progressDay():void {
			timeOfDay += 1;
			timeOfDay %= NIGHT_TO_MORNING + 1;
		}	
		
		public function inTransition():Boolean {
			if (timeOfDay == MORNING || timeOfDay == AFTERNOON) {
				return false;
			} else {
				return true;
			}
		}
	
		
		override public function update():void {
			refresh();
		}
		
		private function refresh():void {
			if (timeOfDay == MORNING) {
				sunImage.x = -75;
			} 
			
			if (timeOfDay == MORNING_TO_AFTERNOON) {
				sunImage.x += 10;
				if (sunImage.x > 675) {
					timeOfDay = AFTERNOON;
					onAnimationEnd();
				}
			}
			
			if (timeOfDay == AFTERNOON) {
				sunImage.x = 675;
			}
			
			if (timeOfDay == AFTERNOON_TO_NIGHT) {
				blackRectangle.alpha += 0.01;
				if (blackRectangle.alpha >= 1) {
					timeOfDay = NIGHT;
					dayText.text = "Day " + (gameState.turnsPlayed / 2 + 1);
				}
			}
			
			if (timeOfDay == NIGHT) {
				
				sunImage.x = -75;
				
				dayText.alpha += 0.01;
				if(dayText.alpha >= 1) {
					timeOfDay = NIGHT_TO_MORNING;
				}
			}
			
			if (timeOfDay == NIGHT_TO_MORNING) {
				
				if(dayText.alpha > 0) {
					dayText.alpha -= 0.02;
				}
				
				blackRectangle.alpha -= 0.01;
				if (blackRectangle.alpha <= 0) {
					timeOfDay = MORNING;
					onAnimationEnd();
				}
			}
			
			
		}
	}
	
}