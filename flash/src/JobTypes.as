package {

	public class JobTypes {
		public static var idle:Job = new Job("Idle",
		"This means doing nothing",
		Resources.DEFAULT_PORTRAIT);
		
		public static var waterCollecting:Job = new Job("Water Collecting",
		"Water is needed for the group to survive.\n"
		+ "If there is not enough water, people\n"
		+ "cannot do their jobs.",
		Resources.WATER_JOB);
		
		public static var fruitGathering:Job = new Job("Fruit Gathering",
		"Fruits increase blood sugar and can be\n"
		+ "found in the forest. This island seems\n"
		+ "to have a lot of bananas.",
		Resources.FRUITS_JOB);
		
		public static var fishing:Job = new Job("Fishing", 
		"Fish do not increase blood sugar levels.\n"
		+ "Many fish can be caught around the shores\n"
		+ "of the island.",
		Resources.FISHING_JOB);
		
		public static var guarding:Job = new Job("Guarding",
		"Monkeys will often invade, stealing things \n"
		+ "and damaging property. To scare the\n"
		+ "monkeys away, guards are needed.",
		Resources.GUARDING_JOB)
		
		public static var raftBuilding:Job = new Job("Raft Building",
		"A raft seems to be the best way to escape\n"
		+ "the island. But building it will take a\n"
		+ "lot of time.",
		Resources.RAFT_JOB);
		
		public static var list:Array = [idle, waterCollecting, fruitGathering, fishing, guarding, raftBuilding];
		
	}
	
}