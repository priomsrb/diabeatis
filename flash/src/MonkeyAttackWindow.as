package 
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import net.flashpunk.graphics.Text;
	import punk.ui.PunkText;
	import punk.ui.PunkWindow;
	import punk.ui.PunkButton;
	import net.flashpunk.FP;
	import punk.ui.PunkLabel;

	
	public class MonkeyAttackWindow extends Window
	{
		private var gameState:GameState;
		
		private var messageText:Text;

		private var raftDamagedLabel:PunkLabel;
		private var waterStolenLabel:PunkLabel;
		private var applesStolenLabel:PunkLabel;
		private var bananasStolenLabel:PunkLabel;
		private var fishStolenLabel:PunkLabel;
				
		public function MonkeyAttackWindow(gameState:GameState) {
			super(200, 80, 400, 400, "Monkey Attack");
			this.gameState = gameState;
		}
		
		override public function added():void {
			add(raftDamagedLabel = new PunkLabel("", 10, 90));
			add(applesStolenLabel = new PunkLabel("", 10, 120));
			add(bananasStolenLabel = new PunkLabel("", 10, 150));
			add(fishStolenLabel = new PunkLabel("", 10, 180));
			add(waterStolenLabel = new PunkLabel("", 10, 210));		
			
			refresh();
		}
		
		public function refresh():void {
			var messageString:String = "Oh no! The monkeys have attacked the camp!\n\n";
			if (gameState.numPeopleDoingJob(JobTypes.guarding) >= 3) {
				messageString += "But the camp guards were able to scare \nall the monkeys away!";
				raftDamagedLabel.text = "";
				waterStolenLabel.text = "";
				applesStolenLabel.text = "";
				bananasStolenLabel.text = "";
				fishStolenLabel.text = "";
			} else {
				raftDamagedLabel.text = "Raft damaged: " + String(gameState.raftDamaged) + "%";
				waterStolenLabel.text = "Water Stolen: " + String(gameState.waterStolen);
				applesStolenLabel.text = "Apples Stolen: " + String(gameState.applesStolen);
				bananasStolenLabel.text = "Bananas Stolen: " + String(gameState.bananasStolen);
				fishStolenLabel.text = "Fish Stolen: " + String(gameState.fishStolen);
			}
			
			if (messageText != null) removeGraphic(messageText);
			messageText = new Text(messageString, 10, 35); 
			messageText.color = 0x000000;
			addGraphic(messageText);
			
			
		}
		
		override public function show():void {
			super.show();
			refresh();
		}
	}

}