package {
	import flash.utils.IDataOutput;
	import flash.utils.IDataInput;
	import flash.utils.IExternalizable;
	import net.flashpunk.graphics.Image;
	import flash.net.registerClassAlias;
	
	[RemoteClass(alias="Person")]
	public class Person implements IExternalizable {
		public var name:String;
		public var bloodSugar:Number = 50;
		public var imageSource:Class = Resources.DEFAULT_PORTRAIT;
		public var imageTint:uint = 0x0;
		public var diet:Food = FoodTypes.airlineMeal;
		public var insulinInjected:Boolean = false;
		public var job:Job = JobTypes.idle;
		
		public function Person(name:String="") {
			this.name = name;
		}
		
		public function doTurn() :void{
			bloodSugar += diet.bloodSugarEffect;
						
			if (insulinInjected) {
				bloodSugar -= 35;
				insulinInjected = false;
			} else {
				bloodSugar -= 5;
			}
			
			bloodSugar = Math.max(Math.min(bloodSugar, 100), 0);	// Keep between 0-100
		}
		
		public function canWork():Boolean {
			if (bloodSugar > 10 && bloodSugar < 90 && diet != FoodTypes.nothing) {
				return true;
			} else {
				return false;
			}
		}
		
		public function writeExternal(output:IDataOutput):void {
			output.writeUTF(name);
			output.writeFloat(bloodSugar);
			output.writeUnsignedInt(imageTint);
			output.writeBoolean(insulinInjected);
			output.writeUTF(diet.name);
			output.writeUTF(job.name);
			
		}

		public function readExternal(input:IDataInput):void {
			name = input.readUTF();
			bloodSugar = input.readFloat();
			imageTint = input.readUnsignedInt();
			insulinInjected = input.readBoolean();
			var dietName:String = input.readUTF();
			for each(var food:Food in FoodTypes.list) {
				if (dietName == food.name) {
					diet = food;
					break;
				}
			}
			
			var jobName:String = input.readUTF();
			for each(var _job:Job in JobTypes.list) {
				if (jobName == _job.name) {
					job = _job;
					break;
				}
			}
			
			
			imageSource = Resources.DEFAULT_PORTRAIT;
		}
	}
	
	registerClassAlias("Person", Person);
}