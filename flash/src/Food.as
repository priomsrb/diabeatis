package {

	public class Food {
		public var name:String;
		public var bloodSugarEffect:Number;
		
		public function Food(name:String="", bloodSugarEffect:Number=0) {
			this.name = name;
			this.bloodSugarEffect = bloodSugarEffect;
		}
	}
}