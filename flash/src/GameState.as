package {
	
	import flash.utils.IDataOutput;
	import flash.utils.IDataInput;
	import flash.utils.IExternalizable;
	import flash.net.registerClassAlias;
	
	[RemoteClass(alias = "GameState")]
	
	public class GameState {
		public const NUM_PLAYERS:int = 10;
		public const MONKEY_ATTACK_CHANCE:Number = 0.1;
		public var waterAmount:int;
		public var airlineMealAmount:int;
		public var appleAmount:int;
		public var bananaAmount:int;
		public var fishAmount:int;
		public var insulinAmount:int;
		public var raftCompletionPercentage:Number = 0;
		
		public var monkeysAttacked:Boolean = false;
		public var waterStolen:int;
		public var applesStolen:int;
		public var bananasStolen:int;
		public var fishStolen:int;
		public var raftDamaged:Number;
		
		public var personList: Vector.<Person> = new Vector.<Person>;
		
		public var turnsPlayed:int;
		
		public function GameState() {
			init();
		}
		
		private function init() : void {
			waterAmount = NUM_PLAYERS * 10;
			airlineMealAmount = NUM_PLAYERS * 3;
			appleAmount = NUM_PLAYERS * 0;
			bananaAmount = NUM_PLAYERS * 0;
			fishAmount = NUM_PLAYERS / 0;
			insulinAmount = NUM_PLAYERS;
			
			turnsPlayed = 0;
			
			for (var i:int = 0; i < NUM_PLAYERS; i++) {
				personList[personList.length] = PersonGenerator.generatePerson();
			}		
		}
		
		public function doTurn() : void {
			var person:Person;
			
			turnsPlayed++;
			
			// Gather water
			for each(person in personList) {
				if (person.canWork() && person.job == JobTypes.waterCollecting) {
					waterAmount += 4;
				}
			}
			
			// Do other jobs if water is available
			for each(person in  personList) {
				if(person.canWork() && waterAmount > 0) {
					if (person.job == JobTypes.fruitGathering) {
						bananaAmount += 2;
						appleAmount += 2;
					} else if (person.job == JobTypes.fishing) {
						fishAmount += 4;
					} else if (person.job == JobTypes.raftBuilding) {
						raftCompletionPercentage += 1.0;
					}
				}
				
				if(waterAmount > 0) {
					waterAmount--;
				}
			}
			
			// Consume resources and adjust blood sugar
			for each(person in  personList) {
				
				if (person.diet == FoodTypes.airlineMeal) {
					if(airlineMealAmount > 0) {
						airlineMealAmount--;
					} else {
						person.diet = FoodTypes.nothing;
					}
				} else if (person.diet == FoodTypes.apple) {
					if(appleAmount > 0) {
						appleAmount--;
					} else {
						person.diet = FoodTypes.nothing;
					}
				} else if (person.diet == FoodTypes.banana) {
					if(bananaAmount > 0) {
						bananaAmount--;
					} else {
						person.diet = FoodTypes.nothing;
					}
				} else if (person.diet == FoodTypes.fish) {
					if(fishAmount > 0) {
						fishAmount--;
					} else {
						person.diet = FoodTypes.nothing;
					}
				}
				
				if (person.insulinInjected) {
					if (insulinAmount > 0) {
						insulinAmount--;
					} else {
						person.insulinInjected = false;
					}
				}
				
				
				person.doTurn();
			}
			
			if (Math.random() <= MONKEY_ATTACK_CHANCE) {
				monkeysAttacked = true;
			} else {
				monkeysAttacked = false;
			}
			
			if (monkeysAttacked) {
				var peopleGuarding:int = numPeopleDoingJob(JobTypes.guarding);
				
				var severity:Number;
				if (peopleGuarding == 0) {
					severity = 1;
				} else if (peopleGuarding < 3) {
					severity = 1 / (peopleGuarding+1);
				} else {
					severity = 0;
				}
				
				waterStolen = Math.min(20 * severity, waterAmount);
				applesStolen = Math.min(10 * severity, appleAmount);
				bananasStolen = Math.min(10 * severity, bananaAmount);
				fishStolen = Math.min(20 * severity, fishAmount);
				raftDamaged = Math.min(12 * severity, raftCompletionPercentage);
				
				waterAmount -= waterStolen;
				appleAmount -= applesStolen;
				bananaAmount -= bananasStolen;
				fishAmount -= fishStolen;
				raftCompletionPercentage -= raftDamaged;
			} else {
				waterStolen = 0;
				applesStolen = 0;
				bananasStolen = 0;
				fishStolen = 0;
				raftDamaged = 0;
			}
		}
		
		public function numPeopleDoingJob(job:Job):int {
			var num:int = 0;
			for each(var person:Person in personList) {
				if (person.job == job) {
					num++;
				}
			}
			
			return num;
		}
	
		private function random(min:int, max:int):int {
			return min + Math.floor(Math.random() * (max+1));
		}
	}
	registerClassAlias("GameState", GameState);
}

