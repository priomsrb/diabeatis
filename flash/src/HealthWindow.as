package 
{
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Canvas;

	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Text;
	import punk.ui.*;
	
	public class HealthWindow extends Window
	{
		public var currentPerson:Person;
		
		private var profileImage: Image;
		private var nameText: Text;
		private var jobText: Text;
		private var statusText: Text;
		private var bloodSugarText: Text;
		private var bloodSugarBar: Canvas;
		private var nextMealText: Text;
		
		private var foodRadioGroup:PunkRadioButtonGroup = new PunkRadioButtonGroup;
		private var airlineMealCheckbox: PunkRadioButton;
		private var appleCheckbox: PunkRadioButton;
		private var bananaCheckbox: PunkRadioButton;
		private var fishCheckbox: PunkRadioButton;
		private var nothingCheckbox: PunkRadioButton;
		private var injectInsulinButton: PunkToggleButton;
		
		private var personScrollList: ScrollList;
		private var gameState: GameState;
	
		
		public function HealthWindow(gameState:GameState)	{
			super(10, 10, 780, 480, "Manage Diabetes");
			this.gameState = gameState;
			currentPerson = gameState.personList[0];
		}
		
		override public function added():void {
			profileImage = new Image(currentPerson.imageSource);
			profileImage.color = currentPerson.imageTint;
			profileImage.x = 415;
			profileImage.y = 50;
			addGraphic(profileImage);
			
			Text.size = 16;

			bloodSugarText = new Text("Blood Sugar: ", 415, 160);
			bloodSugarText.color = 0x000000;
			addGraphic(bloodSugarText);
			
			bloodSugarBar = new Canvas(105, 20);
			bloodSugarBar.x = 530;
			bloodSugarBar.y = 160;
			addGraphic(bloodSugarBar);
			
			
			nextMealText = new Text("Next Meal: ", 415, 195);
			nextMealText.color = 0x000000;
			addGraphic(nextMealText);

			
			airlineMealCheckbox = new PunkRadioButton(foodRadioGroup, "", 425, 230, 200, 30, false, "Airline Meal", setDietToAirlineMeal);
			appleCheckbox = new PunkRadioButton(foodRadioGroup, "", 630, 230, 200, 30, false, "Apple", setDietToApple);
			bananaCheckbox = new PunkRadioButton(foodRadioGroup, "", 425, 275, 200, 30, false, "Banana", setDietToBanana);
			fishCheckbox = new PunkRadioButton(foodRadioGroup, "", 630, 275, 200, 30, false, "Fish", setDietToFish)
			nothingCheckbox = new PunkRadioButton(foodRadioGroup, "", 425, 320, 200, 30, true, "Nothing", setDietToNothing)
			add(airlineMealCheckbox);
			add(appleCheckbox);
			add(bananaCheckbox);
			add(fishCheckbox);
			add(nothingCheckbox);
			
			injectInsulinButton = new PunkToggleButton(425, 400, 130, 40, false, "Inject Insulin", injectInsulinToggled);
			add(injectInsulinButton);
			
			personScrollList = new ScrollList(15, 45, 370, 420, this);
			add(personScrollList);
			
			for (var i:int = 0; i < gameState.personList.length; i++) {
				personScrollList.add(new PersonListButton(gameState.personList[i], 10, 10, 330, 80, setPerson));
			}
			
	
			refresh();
		}
		
		
		public function refresh():void {
			removeGraphic(profileImage);
			profileImage = new Image(Resources.DEFAULT_PORTRAIT);
			profileImage.color = currentPerson.imageTint;
			profileImage.x = 415;
			profileImage.y = 50;
			addGraphic(profileImage);
			
			if (nameText != null) removeGraphic(nameText);
			nameText = new Text(currentPerson.name, 530, 50); 
			nameText.color = 0x000000;
			addGraphic(nameText);
			
			if (jobText != null) removeGraphic(jobText);
			jobText = new Text(currentPerson.job.name, 530, 80); 
			if (currentPerson.job == JobTypes.idle) {
				jobText.color = 0xFF0000;
			} else {
				jobText.color = 0x00FF00;
			}
			addGraphic(jobText);
			
			if (statusText != null) removeGraphic(statusText);
			if (!currentPerson.canWork()) {
				statusText = new Text("Cannot do job", 530, 110); 
				statusText.color = 0xFF0000;
				addGraphic(statusText);
			}
			
			bloodSugarBar.drawRect(new Rectangle(0, 0, 105, 20), 0x888888);
			var bloodSugarDanger:Number = Math.abs(50 - currentPerson.bloodSugar) / 50.0;
			var bloodSugarColor:uint = FP.getColorHSV(0.3-bloodSugarDanger*0.3,1,1);
			bloodSugarBar.drawRect(new Rectangle(0, 0, 5+currentPerson.bloodSugar, 20), bloodSugarColor);
			
			if (currentPerson.diet == FoodTypes.airlineMeal) {
				foodRadioGroup.toggleOn(airlineMealCheckbox);
			} else if (currentPerson.diet == FoodTypes.apple) {
				foodRadioGroup.toggleOn(appleCheckbox);
			} else if (currentPerson.diet == FoodTypes.banana) {
				foodRadioGroup.toggleOn(bananaCheckbox);
			} else if (currentPerson.diet == FoodTypes.fish) {
				foodRadioGroup.toggleOn(fishCheckbox);
			} else if (currentPerson.diet == FoodTypes.nothing) {
				foodRadioGroup.toggleOn(nothingCheckbox);
			}
			
			airlineMealCheckbox.label.text = "Airline Meal X " + gameState.airlineMealAmount.toString();
			appleCheckbox.label.text = "Apple X " + gameState.appleAmount.toString();
			bananaCheckbox.label.text = "Banana X " + gameState.bananaAmount.toString();
			fishCheckbox.label.text = "Fish X " + gameState.fishAmount.toString();
			injectInsulinButton.label.text = "Inject Insulin X " + gameState.insulinAmount;
			
			
			injectInsulinButton.on = currentPerson.insulinInjected;
		}
		
		
		private function setDietToAirlineMeal(on:Boolean) : void {
			if (!on) return;
			currentPerson.diet = FoodTypes.airlineMeal;
			refresh();
		}		
		
		private function setDietToApple(on:Boolean) : void {
			if (!on) return;
			currentPerson.diet = FoodTypes.apple;
			refresh();
		}
		
		private function setDietToBanana(on:Boolean) : void {
			if (!on) return;
			currentPerson.diet = FoodTypes.banana;
			refresh();
		}		
		
		private function setDietToFish(on:Boolean) : void {
			if (!on) return;
			currentPerson.diet = FoodTypes.fish;
			refresh();
		}		
		
		private function setDietToNothing(on:Boolean) : void {
			if (!on) return;
			currentPerson.diet = FoodTypes.nothing;
			refresh();
		}
		
		private function injectInsulinToggled(on:Boolean) : void {
			currentPerson.insulinInjected = on;
			refresh();
		}
		
		private function setPerson(person: Person) : void {
			this.currentPerson = person;
			refresh();
		}
		
		override public function show():void 
		{
			super.show();
			refresh();
		}
		
		
	}

}