package 
{
	import flash.geom.Rectangle;
	import net.flashpunk.graphics.Canvas;

	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Text;
	import punk.ui.*;
	
	public class JobsWindow extends Window
	{
		public var currentJob:Job;
		
		private var currentJobImage: Image;
		private var nameText: Text;
		private var descriptionText: Text;
		private var numberOfPeopleAssignedText: Text;
		private var assignLessPeopleButton: PunkButton;
		private var assignMorePeopleButton: PunkButton;
		private var numberOfPeopleAvailableText: Text;
		
		private var jobScrollList: ScrollList;
		
		private var gameState:GameState;
		//private var personList: Vector.<Person>;
	
		
		public function JobsWindow(gameState:GameState) {
			super(10, 10, 780, 480, "Manage Jobs");
			currentJob = JobTypes.waterCollecting;
			this.gameState = gameState;
		}
		
		override public function added():void {
			currentJobImage = new Image(currentJob.imageSource);
			currentJobImage.x = 415;
			currentJobImage.y = 50;
			addGraphic(currentJobImage);
			
			Text.size = 16;
			var peopleAssignedText:Text = new Text("People assigned: ", 415, 255);
			peopleAssignedText.color = 0x000000;
			addGraphic(peopleAssignedText);
			
			numberOfPeopleAssignedText = new Text("0  ", 565, 300);
			numberOfPeopleAssignedText.color = 0x000000;
			addGraphic(numberOfPeopleAssignedText);

			assignLessPeopleButton = new PunkButton(510, 295, 35, 35, "-", assignLessPeopleToJob);
			add(assignLessPeopleButton);
			
			assignMorePeopleButton = new PunkButton(600, 295, 35, 35, "+", assignMorePeopleToJob);
			add(assignMorePeopleButton);
			
			var peopleAvailableText:Text = new Text("People available: ", 415, 360);
			peopleAvailableText.color = 0x000000;
			addGraphic(peopleAvailableText);
			
			
			numberOfPeopleAvailableText = new Text("20", 560, 360);
			numberOfPeopleAvailableText.color = 0x000000;
			addGraphic(numberOfPeopleAvailableText);
			
			jobScrollList = new ScrollList(15, 45, 370, 420, this);
			add(jobScrollList);
			
			
			jobScrollList.add(new JobListButton(JobTypes.waterCollecting, 0, 0, 330, 80, setJob));
			jobScrollList.add(new JobListButton(JobTypes.fruitGathering, 0, 0, 330, 80, setJob));
			jobScrollList.add(new JobListButton(JobTypes.fishing, 0, 0, 330, 80, setJob));
			jobScrollList.add(new JobListButton(JobTypes.guarding, 0, 0, 330, 80, setJob));
			jobScrollList.add(new JobListButton(JobTypes.raftBuilding, 0, 0, 330, 80, setJob));				
	
			refresh();
		}
		
		
		public function refresh():void {
			removeGraphic(currentJobImage);
			currentJobImage = new Image(currentJob.imageSource);
			currentJobImage.x = 415;
			currentJobImage.y = 50;
			addGraphic(currentJobImage);
			
			Text.size = 18;
			if (nameText != null) removeGraphic(nameText);
			nameText = new Text(currentJob.name, 530, 50); 
			nameText.color = 0x000000;
			addGraphic(nameText);
			
			Text.size = 16;
			if(descriptionText != null) removeGraphic(descriptionText);
			descriptionText = new Text(currentJob.description, 415, 160);
			descriptionText.color = 0x000000;
			addGraphic(descriptionText);
			
			var numberOfPeopleAssigned:int = 0;
			var numberOfPeopleAvailable:int = 0;
			for each(var person:Person in gameState.personList) {
				if (person.job == currentJob) {
					numberOfPeopleAssigned++;
				} else if(person.job == JobTypes.idle) {
					numberOfPeopleAvailable++;
				}
			}
			
			numberOfPeopleAssignedText.text = numberOfPeopleAssigned.toString();
			numberOfPeopleAvailableText.text = numberOfPeopleAvailable.toString();
		}
		
		private function setJob(job: Job) : void {
			this.currentJob = job;
			refresh();
		}
		
		private function assignLessPeopleToJob() : void {
			for each(var person:Person in gameState.personList) {
				if (person.job == currentJob) {
					person.job = JobTypes.idle;
					break;
				}
			}
			
			refresh();
		}
		
		private function assignMorePeopleToJob() : void {
			for each(var person:Person in gameState.personList) {
				if (person.job == JobTypes.idle) {
					person.job = currentJob;
					break;
				}
			}
			
			refresh();
		}
		
		
	}

}