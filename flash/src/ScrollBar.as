package 
{
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	
	import punk.ui.skin.PunkSkinButtonElement;
	import punk.ui.skin.PunkSkin;
	import punk.ui.skin.PunkSkinImage;
	import punk.ui.*;

	/**
	 * A button component.
	 */
	public class ScrollBar extends PunkUIComponent
	{	

		
		/**
		 * Function called when the button is released
		 */
		public var onReleased:Function = null;
		
		/**
		 * Function called when the button is dragged
		 */
		public var onChanged:Function = null;

		/**
		 * Is the button pressed
		 */
		protected var isPressed:Boolean = false;
		
		/**
		 * Is the button activated via the mouse
		 */
		protected var isMoused:Boolean = false;

		/**
		 * Graphic of the button when it's active and it's not being pressed and the mouse is outside of it.
		 */	
		public var normalGraphic:Graphic = new Graphic;
		/**
		 * Graphic of the button when the mouse overs it and it's active.
		 */		
		public var mousedGraphic:Graphic = new Graphic;
		/**
		 * Graphic of the button when the mouse is pressing it and it's active.
		 */		
		public var pressedGraphic:Graphic = new Graphic;
		/**
		 * Graphic of the button when inactive.
		 */
		public var inactiveGraphic:Graphic = new Graphic;
		
		public var backgroundGraphic:Graphic = new Graphic;
		
		protected var _buttonHeight:int = 1;
		protected var scrollPosition:int = 0;
		protected var clickOffset:int = 0;
		protected var skin:PunkSkin = PunkUI.skin;
		
	
		/**
		 * Has the component been inititalized
		 */
		protected var initialised:Boolean = false;
		
		/**
		 * Constructor
		 *  
		 * @param x					The x coordinate of the scroll bar
		 * @param y					The y coordinate of the scroll bar
		 * @param width				The width of the scroll bar
		 * @param height			The height of the scroll bar
		 * @param onChanged			What to do when the scroll progress is changed
		 * @param hotkey            Hotkey the trigger the component
		 * @param skin              The skin to use when rendering the component
		 */		
		public function ScrollBar(x:Number=0, y:Number=0, width:int=1, height:int=1, onChanged:Function=null, skin:PunkSkin = null) {
			super(x, y, width, height, skin);
			this.skin = skin ? skin : PunkUI.skin;
			buttonHeight = height / 5;
			
			this.onChanged = onChanged;
		}
		
		/**
		 * Additional setup steps for this component
		 * @param	skin Skin to use when rendering the component
		 */
		override protected function setupSkin(skin:PunkSkin):void
		{
			if(!skin.punkButton) return;
			
			setUpButtonSkin(skin.punkButton);
			
			backgroundGraphic = getSkinImage(skin.punkButton.inactive);
		}
		
		/**
		 * Additional setup specifically for the button's graphical states
		 * @param	skin Skin to use when rendering the component
		 */
		protected function setUpButtonSkin(skin:PunkSkinButtonElement):void
		{
			if(!skin) return;
			
			this.normalGraphic = getSkinImage(skin.normal, 0, buttonHeight);
			var mousedGraphic:Image = getSkinImage(skin.moused, 0, buttonHeight);
			this.mousedGraphic = mousedGraphic ? mousedGraphic : normalGraphic;
			var pressedGraphic:Image = getSkinImage(skin.pressed, 0, buttonHeight);
			this.pressedGraphic = pressedGraphic ? pressedGraphic : normalGraphic;
			var inactiveGraphic:Image = getSkinImage(skin.inactive, 0, buttonHeight);
			this.inactiveGraphic = inactiveGraphic ? inactiveGraphic : normalGraphic;
		}
		
		/**
		 * Setup the different callbacks that this component uses
		 * @param	onReleased Function called when mouse is release
		 * @param	onChanged Function called when scroll progress has changed

		 */
		public function setCallbacks(onReleased:Function=null, onChanged:Function=null) : void
		{
			this.onReleased = onReleased;
			this.onChanged = onChanged;
		}
		
		/**
		 * @private 
		 */
		override public function update():void{
			super.update();
			
			if(!initialised)
			{
				if(FP.stage) {
					FP.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false, 0, true);
					FP.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp, false, 0, true);
					initialised = true;
				}
			}
			
			if(PunkUI.mouseIsOver(this, true) && FP.world.mouseY >= y+scrollPosition && FP.world.mouseY <= y+scrollPosition+buttonHeight)
			{
				isMoused = true;
				_currentGraphic = 1;
			}
			else
			{
				isMoused = false;
				_currentGraphic = 0;
			}
			
			if (isPressed) {
				_currentGraphic = 2;
			}
			
			if (isPressed) {
				scrollPosition = FP.world.mouseY - y + clickOffset;
				scrollPosition = Math.max(0, Math.min(height - buttonHeight, scrollPosition));
				scrollProgress = scrollPosition / (height - buttonHeight);
			}
		}
		
		/**
		 * @private
		 */
		override public function render():void {
			
			renderGraphic(backgroundGraphic);
			
			if(active)
			{
				switch(_currentGraphic)
				{
					case 0:
						renderGraphic(normalGraphic);
						break;
					case 1:
						renderGraphic(mousedGraphic);
						break;
					case 2:
						renderGraphic(pressedGraphic);
						break;
				}
			}
			else
			{
				renderGraphic(inactiveGraphic);
			}

		}

		
		/**
		 * helper function to ensure the validity of a call to fire the onRelease function
		 */
		protected function releasedCallback():void
		{
			isPressed = false;
			if(onReleased != null) onReleased();
		}

	
		/**
		 * @private
		 */		
		protected function onMouseDown(e:MouseEvent = null):void {
			clickOffset = y + scrollPosition - FP.world.mouseY;
			if (!active || !Input.mousePressed || isPressed) return;
			if(isMoused) isPressed = true;
		}
		
		/**
		 * @private
		 */		
		protected function onMouseUp(e:MouseEvent = null):void {
			if(!active || !Input.mouseReleased || !isPressed) return;
			isPressed = false;
			if(isMoused) releasedCallback();
		}
		
		/**
		 * @private
		 */
		override public function added():void {
			super.added();
			
			initialised = false;
			
			if(FP.stage) {
				FP.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false, 0, true);
				FP.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp, false, 0, true);
				initialised = true;
			}
		}
		
		/**
		 * @private
		 */
		override public function removed():void {
			super.removed();
			
			if(FP.stage) {
				FP.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				FP.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			}
		}
		
		public function set scrollProgress(value:Number):void {
			if(onChanged != null) onChanged();
			scrollPosition = value * (height - buttonHeight);
			scrollPosition = Math.max(0, Math.min(height - buttonHeight, scrollPosition));
			moveButtonIntoPosition();
		}
		
		public function get scrollProgress() : Number {
			if (buttonHeight == height) {
				return 0;	// Avoid division by 0
			} else {
				return scrollPosition / (height - buttonHeight);
			}
		}
		
		public function get buttonHeight() : int {			
			return _buttonHeight;
		}
		
		public function set buttonHeight(height:int) : void {			
			_buttonHeight = height;
			setUpButtonSkin(skin.punkButton);
			scrollProgress = scrollProgress; // Recalculate scroll position
		}
		
		private function moveButtonIntoPosition():void {
			normalGraphic.y = scrollPosition;
			pressedGraphic.y = scrollPosition;
			mousedGraphic.y = scrollPosition;
			inactiveGraphic.y = scrollPosition;
		}
		
		protected var _currentGraphic:int = 0;
	}
}
