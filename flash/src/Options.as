package {
	
	import flash.net.SharedObject;
	import net.flashpunk.FP;
	
	public class Options {
		private static var _mute:Boolean = false;
		

		public static function set mute(m:Boolean):void {
			_mute = m;
			if (_mute) {
				FP.volume = 0;
			} else {
				FP.volume = 1;
			}
			
			save();
		}
		
		public static function get mute():Boolean {
			return _mute;
		}
		
		
		
		public static function load():void {
			var so:SharedObject = SharedObject.getLocal("options");
			if(so.size != 0) {
				mute = so.data.mute;
			}
		}
		
		public static function save():void {
			var so:SharedObject = SharedObject.getLocal("options");
			so.data.mute = mute;
			so.flush();
		}
		

	}
	
}