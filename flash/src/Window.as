package 
{
	import punk.ui.PunkWindow;
	import punk.ui.PunkButton;
	import net.flashpunk.FP;
	
	
	public class Window extends PunkWindow
	{
		
		public var onHideCallback:Function;
		
		public function Window(x:int, y:int, width:int, height:int, caption:String) {
			super(x, y, width, height, caption, false);
			
			add(new PunkButton(width - 30, 0, 30, 30, "X", hide));
		}
		
		override public function added():void {

		}
		
		public function hide():void {
			if(visible) {
				visible = false;
				active = false;
				
				if (onHideCallback != null) onHideCallback();
			}
		}
		
		public function show():void {
			if(!visible) {
				visible = true;
				active = true;
			}
		}
		
		public function close():void {
			FP.world.remove(this);
		}
		
	}

}