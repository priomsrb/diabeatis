package 
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import net.flashpunk.FP;
	
	import punk.ui.skin.PunkSkin;
	import punk.ui.skin.PunkSkinButtonElement;
	import punk.ui.skin.PunkSkinHasLabelElement;
	import punk.ui.skin.PunkSkinImage;
	import punk.ui.skin.PunkSkinLabelElement;
	import punk.ui.skin.PunkSkinToggleButtonElement;
	import punk.ui.skin.PunkSkinWindowElement;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Rolpege Blue skin definition
	 */		
	public class GameSkin extends PunkSkin
	{
		/**
		 * The asset to use for the skin image. 
		 */		
		[Embed(source="assets/game_skin.png")] protected const I:Class;
		
		/**
		 * Constructor. 
		 */		
		public function GameSkin()
		{
			super();
			
			punkButton = new PunkSkinButtonElement(gy(0, 0), gy(30, 0), gy(60, 0), gy(90, 0), {font: Text.font, color: 0x000000, size: 18});
			//punkToggleButton = new PunkSkinToggleButtonElement(gy(0, 0), gy(30, 0), gy(60, 0), gy(60, 0), gy(0, 30), gy(30, 30), gy(60, 30), gy(60, 30), { color: 0x000000, size: 16, align: "center" } );
			punkToggleButton = new PunkSkinToggleButtonElement(gn(0, 90), gn(30, 90), gn(60, 90), gn(0, 90), gn(0, 120), gn(30, 120), gn(60, 120), gn(0, 120), {font: Text.font, color: 0x000000, size: 16, x: 37, y: 3});
			punkRadioButton = new PunkSkinToggleButtonElement(gn(0, 90), gn(30, 90), gn(60, 90), gn(0, 90), gn(0, 120), gn(30, 120), gn(60, 120), gn(0, 120), {font: Text.font, color: 0x000000, size: 16, x: 37, y: 3});
			
			punkLabel = new PunkSkinHasLabelElement({font: Text.font, color: 0x000000, size: 16});
			punkTextArea = new PunkSkinLabelElement({font: Text.font, color: 0x000000, size: 16, x: 4}, gy(60, 0));
			punkTextField = new PunkSkinLabelElement({font: Text.font, color: 0x000000, size: 16, x: 4}, gy(60, 0));
			punkPasswordField = new PunkSkinLabelElement({font: Text.font, color: 0x000000, size: 16, x: 4}, gy(60, 0));
			
			punkWindow = new PunkSkinWindowElement(gy(30, 60), gy(0, 60), {font: Text.font, color: 0x000000, size: 16, x: 8, y: 5});
		}
		
		/**
		 * Returns the portion of the skin image as a PunkSkinImage object in a 9-Slice format
		 * @param	x X-Coordinate for the image offset
		 * @param	y Y-Coordinate for the image offset
		 * @param	w Width of the image sub-section
		 * @param	h Height of the image sub-section
		 * @return PunkSkinImage for the image sub-section requested in 9-Slice format
		 */
		protected function gy(x:int, y:int, w:int=30, h:int=30):PunkSkinImage
		{
			return new PunkSkinImage(gi(x, y, w, h), true, w/2, w/2 - 1, h/2, h/2 - 1);
		}
		
		/**
		 * Returns the portion of the skin image as a PunkSkinImage object in a non 9-Sliced format
		 * @param	x X-Coordinate for the image offset
		 * @param	y Y-Coordinate for the image offset
		 * @param	w Width of the image sub-section
		 * @param	h Height of the image sub-section
		 * @return PunkSkinImage for the image sub-section requested in a non 9-Sliced format
		 */
		protected function gn(x:int, y:int, w:int=30, h:int=30):PunkSkinImage
		{
			return new PunkSkinImage(gi(x, y, w, h), false);
		}
		
		/**
		 * Returns the portion of the skin image requested as a BitmapData object
		 * @param	x X-Coordinate for the image offset
		 * @param	y Y-Coordinate for the image offset
		 * @param	w Width of the image sub-section
		 * @param	h Height of the image sub-section
		 * @return BitmapData for the image sub-section requested
		 */
		protected function gi(x:int, y:int, w:int=30, h:int=30):BitmapData
		{
			_r.x = x;
			_r.y = y;
			_r.width = w;
			_r.height = h;
			
			var b:BitmapData = new BitmapData(w, h, true, 0);
			b.copyPixels(FP.getBitmap(I), _r, FP.zero, null, null, true);
			return b;
		}
		
		private var _r:Rectangle = new Rectangle;
	}
}