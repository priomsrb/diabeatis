package {
	
	import punk.ui.PunkUI;
	import net.flashpunk.graphics.Text;
	
	public final class Resources {
		[Embed(source = 'assets/default.ttf', embedAsCFF = "false", fontFamily = 'normal')] public static const DEFAULT_FONT:Class;
		
		// TODO:remove this
		[Embed(source = 'assets/button_normal.png')] public static const WINDOW_BACKGROUND:Class;
		
		[Embed(source = 'assets/water_job.png')] public static const WATER_JOB:Class;
		[Embed(source = 'assets/fruits_job.png')] public static const FRUITS_JOB:Class;
		[Embed(source = 'assets/fishing_job.png')] public static const FISHING_JOB:Class;
		[Embed(source = 'assets/guarding_job.png')] public static const GUARDING_JOB:Class;
		[Embed(source = 'assets/raft_job.png')] public static const RAFT_JOB:Class;
		
		[Embed(source = 'assets/button_normal.png')] public static const BUTTON_NORMAL:Class;
		[Embed(source = 'assets/button_hover.png')] public static const BUTTON_HOVER:Class;
		[Embed(source = 'assets/button_pressed.png')] public static const BUTTON_PRESSED:Class;
		[Embed(source = 'assets/button_inactive.png')] public static const BUTTON_INACTIVE:Class;
		
		[Embed(source = 'assets/checkbox_checked.png')] public static const CHECKBOX_CHECKED:Class;
		[Embed(source = 'assets/checkbox_unchecked.png')] public static const CHECKBOX_UNCHECKED:Class;
		
		[Embed(source = 'assets/portrait.png')] public static const DEFAULT_PORTRAIT:Class;
		
		//Title Screen
		[Embed(source = 'assets/island.jpg')] public static const DEFAULT_BACKGROUND:Class;
		
		//Sound
		[Embed(source = 'assets/POL-water-world-short.mp3')]public static const DEFAULT_MUSIC:Class;
		
		[Embed(source = 'assets/bright_sun.png')] public static const DEFAULT_SUN:Class;
		[Embed(source = 'assets/airplane.png')] public static const DEFAULT_PLANE:Class;
		[Embed(source = 'assets/needle.png')] public static const DEFAULT_NEEDLE:Class;
		[Embed(source = 'assets/title.png')] public static const TITLE:Class;
				
		//TODO: This hack is needed for loading/saving to work. Maybe figure out a better solution?
		private static var compiledClasses:Array = [ GameState, Person ];
		
		static public function init() : void {
			Text.font = "normal";
			PunkUI.skin = new GameSkin;
		}
		
	}
	

}
