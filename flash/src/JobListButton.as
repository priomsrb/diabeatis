package {
	import punk.ui.PunkButton;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;

	public class JobListButton extends PunkButton {
		
		public var job:Job;
		private var jobImage: Image;
		private var nameText: Text;
		private var callback: Function;
		
		public function JobListButton(job:Job, x:Number = 0, y:Number = 0, width:int = 1, height:int = 1, callback:Function = null) {
			super(x, y, width, height, "", released);
			
			this.job = job;
			this.callback = callback;
			
			jobImage = new Image(job.imageSource);
			jobImage.x = 20;
			jobImage.y = 15;
			jobImage.scale = 0.5
			
			Text.size = 18;
			nameText = new Text(job.name, 85, 25); 
			nameText.color = 0x000000;

		}
		
		override public function render():void {
			super.render();
			
			renderGraphic(jobImage);
			renderGraphic(nameText);
		}
		
		private function released():void {
			if(callback != null)
			callback(job);
		}
	}
}