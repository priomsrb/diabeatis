package 
{
	import flash.net.FileReference;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.ByteArray;
	import punk.ui.PunkWindow;
	import punk.ui.PunkButton;
	import net.flashpunk.FP;
	
	
	public class OptionsWindow extends Window
	{
		
		private var gameState:GameState;
		
		private var muteButton:PunkButton;
		
		public function OptionsWindow(gameState:GameState) {
			super(200, 80, 400, 400, "Options");
			this.gameState = gameState;
			
			add(muteButton = new PunkButton(10, 40, 380, 50, "Mute", toggleMute));
			add(new PunkButton(10, 100, 380, 50, "More About Diabetes", moreInfo));
			add(new PunkButton(10, 320, 380, 50, "Save and Quit", saveAndQuit));
			
			updateMuteButton();
		}
		
		private function saveAndQuit() : void {		
			var so:SharedObject = SharedObject.getLocal("gameState");
			so.clear();
			so.data.gameState = gameState;
			
			FP.world = new TitleWorld;
		}
		
		private function moreInfo() : void {
			navigateToURL(new URLRequest("http://en.wikipedia.org/wiki/Diabetes_mellitus"));
		}
		
		private function toggleMute(): void {
			Options.mute = !Options.mute;
			updateMuteButton();
		}
		
		private function updateMuteButton():void {
			if (Options.mute == true) {
				muteButton.label.text = "Unmute";
			} else {
				muteButton.label.text = "Mute";
			}
		}
	}

}