package {
	import punk.ui.PunkButton;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;

	public class PersonListButton extends PunkButton {
		
		public var person:Person;
		private var profileImage: Image;
		private var nameText: Text;
		private var bloodSugarText: Text;
		private var bloodSugarLevelText: Text;
		private var callback: Function;
		private var lastBloodSugar:Number = -1;
		
		public function PersonListButton(person:Person, x:Number = 0, y:Number = 0, width:int = 1, height:int = 1, callback:Function = null) {
			super(x, y, width, height, "", released);
			
			this.person = person;
			this.callback = callback;
			
			// TODO: make a copy of the person's picture
			profileImage = new Image(person.imageSource);
			profileImage.color = person.imageTint;
			profileImage.x = 20;
			profileImage.y = 15;
			profileImage.scale = 0.5
			
			Text.size = 16;
			nameText = new Text(person.name, 90, 12); 
			nameText.color = 0x000000;
			
			bloodSugarText = new Text("Blood Sugar:", 90, 42); 
			bloodSugarText.color = 0x000000;
			
			refresh();
		}
		
		override public function render():void {
			super.render();
			
			refresh();
			
			renderGraphic(profileImage);
			renderGraphic(nameText);
			renderGraphic(bloodSugarText);
			renderGraphic(bloodSugarLevelText);
		}
		
		private function refresh():void {
			if (person.bloodSugar != lastBloodSugar) {
				var bloodSugarLevelString:String;
				var bloodSugarColor:uint;
			
				if (person.bloodSugar < 10) {
					bloodSugarLevelString = "Too Low";
					bloodSugarColor = 0xFF0000;
				} else if (person.bloodSugar < 30) {
					bloodSugarLevelString = "Low";
					bloodSugarColor = 0xFFFF00;
				} else if (person.bloodSugar < 70) {
					bloodSugarLevelString = "Normal";
					bloodSugarColor = 0x00AA00;
				} else if (person.bloodSugar < 90) {
					bloodSugarLevelString = "High";
					bloodSugarColor = 0xFFFF00;
				} else if (person.bloodSugar >= 90) {
					bloodSugarLevelString = "Too High";
					bloodSugarColor = 0xFF0000;
				} 
				
				Text.size = 16;
				bloodSugarLevelText = new Text(bloodSugarLevelString, 200, 42); 
				bloodSugarLevelText.color = bloodSugarColor;
				lastBloodSugar = person.bloodSugar;
			}
		}
		
		private function released():void {
			if(callback != null)
			callback(person);
		}
	}
}