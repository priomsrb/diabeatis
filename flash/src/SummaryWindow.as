package 
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import net.flashpunk.graphics.Text;
	import punk.ui.PunkText;
	import punk.ui.PunkWindow;
	import punk.ui.PunkButton;
	import net.flashpunk.FP;
	import punk.ui.PunkLabel;
	import mx.utils.ObjectUtil;

	
	public class SummaryWindow extends Window
	{
		private var gameState:GameState;
		private var previousGameState:GameState;
		
		private var dayLabel:PunkLabel;
		private var totalAndChangeLabel:PunkLabel;
		
		private var waterLabel:PunkLabel;
		private var airlineMealsLabel:PunkLabel;
		private var applesLabel:PunkLabel;
		private var bananasLabel:PunkLabel;
		private var fishLabel:PunkLabel;
		private var insulinLabel:PunkLabel;
		private var raftCompletionLabel:PunkLabel;
		
		private var waterChangeText:Text;
		private var airlineMealsChangeText:Text;
		private var applesChangeText:Text;
		private var bananasChangeText:Text;
		private var fishChangeText:Text;
		private var insulinChangeText:Text;
		private var raftCompletionChangeText:Text;

		private var waterTotalText:Text;
		private var airlineMealsTotalText:Text;
		private var applesTotalText:Text;
		private var bananasTotalText:Text;
		private var fishTotalText:Text;
		private var insulinTotalText:Text;
		private var raftCompletionTotalText:Text;
		
		private var didYouKnowLabel:PunkLabel;
		private var didYouKnowText:Text;
		
		protected var labels:Vector.<PunkLabel> = new Vector.<PunkLabel>;
		protected var changeTexts:Vector.<Text> = new Vector.<Text>;
		protected var totalTexts:Vector.<Text> = new Vector.<Text>;
		
		public function SummaryWindow(gameState:GameState) {
			super(200, 30, 400, 450, "Summary");
			this.gameState = gameState;
			previousGameState = ObjectUtil.copy(gameState) as GameState;
		}
		
		override public function added():void {	
			add(waterLabel = new PunkLabel("Water:", 10, 0));
			add(airlineMealsLabel = new PunkLabel("Airline Meals:", 10, 0));
			add(applesLabel = new PunkLabel("Apples:", 10, 0));
			add(bananasLabel = new PunkLabel("Bananas:", 10, 0));
			add(fishLabel = new PunkLabel("Fish:", 10, 0));
			add(insulinLabel = new PunkLabel("Insulin:", 10, 0));
			add(raftCompletionLabel = new PunkLabel("Raft Completion:", 10, 0));
			
			addGraphic(waterTotalText = new Text("00000", 170, 0));
			addGraphic(applesTotalText = new Text("00000", 170, 0));
			addGraphic(bananasTotalText = new Text("00000", 170, 0));
			addGraphic(fishTotalText = new Text("00000", 170, 0));
			addGraphic(insulinTotalText = new Text("00000", 170, 0));
			addGraphic(raftCompletionTotalText = new Text("000%", 170, 0));
			
			addGraphic(waterChangeText = new Text("-000", 250, 0));	
			addGraphic(airlineMealsChangeText = new Text("-000", 250, 0));
			addGraphic(applesChangeText = new Text("-000", 250, 0));
			addGraphic(bananasChangeText = new Text("-000", 250, 0));
			addGraphic(fishChangeText = new Text("-000", 250, 0));
			addGraphic(insulinChangeText = new Text("-000", 250, 0));
			addGraphic(raftCompletionChangeText = new Text("-000%", 250, 0));
			addGraphic(airlineMealsTotalText = new Text("00000", 170, 0));
			
			labels.push(raftCompletionLabel);
			labels.push(waterLabel);
			labels.push(airlineMealsLabel);
			labels.push(applesLabel);
			labels.push(bananasLabel);
			labels.push(fishLabel);
			labels.push(insulinLabel);
			
			totalTexts.push(raftCompletionTotalText);
			totalTexts.push(waterTotalText);
			totalTexts.push(airlineMealsTotalText);
			totalTexts.push(applesTotalText);
			totalTexts.push(bananasTotalText);
			totalTexts.push(fishTotalText);
			totalTexts.push(insulinTotalText);
			
			changeTexts.push(raftCompletionChangeText);
			changeTexts.push(waterChangeText);
			changeTexts.push(airlineMealsChangeText);
			changeTexts.push(applesChangeText);
			changeTexts.push(bananasChangeText);
			changeTexts.push(fishChangeText);
			changeTexts.push(insulinChangeText);
			
			add(dayLabel = new PunkLabel("Day 0: Morning", 10, 35));
			add(totalAndChangeLabel = new PunkLabel("Total       Change", 160, 70));
			add(didYouKnowLabel = new PunkLabel("Did you know?", 10, 340));
			addGraphic(didYouKnowText = new Text(DidYouKnowSentences.sentences[0], 10, 370));
			didYouKnowText.color = 0x000000;
			
			for each(var text:Text in totalTexts) {
				text.color = 0x000000;
			}
		}
		
		private function refresh():void {
			
			
			dayLabel.text = "Day " + Math.round(gameState.turnsPlayed / 2) + ": ";
			if (gameState.turnsPlayed % 2 == 0) {
				dayLabel.text += "Morning";
			} else {
				dayLabel.text += "Afternoon";
			}
			
			var waterChange:int = gameState.waterAmount - previousGameState.waterAmount;
			updateTotalText(waterTotalText, gameState.waterAmount);
			updateChangeText(waterChangeText, waterChange);
			
			var airlineMealsChange:int = gameState.airlineMealAmount - previousGameState.airlineMealAmount;
			updateTotalText(airlineMealsTotalText, gameState.airlineMealAmount);
			updateChangeText(airlineMealsChangeText, airlineMealsChange);
			
			var applesChange:int = gameState.appleAmount - previousGameState.appleAmount;
			updateTotalText(applesTotalText, gameState.appleAmount);
			updateChangeText(applesChangeText, applesChange);
			
			var bananasChange:int = gameState.bananaAmount - previousGameState.bananaAmount;
			updateTotalText(bananasTotalText, gameState.bananaAmount);
			updateChangeText(bananasChangeText, bananasChange);

			var fishChange:int = gameState.fishAmount - previousGameState.fishAmount;
			updateTotalText(fishTotalText, gameState.fishAmount);
			updateChangeText(fishChangeText, fishChange);

			var insulinChange:int = gameState.insulinAmount - previousGameState.insulinAmount;
			updateTotalText(insulinTotalText, gameState.insulinAmount);
			updateChangeText(insulinChangeText, insulinChange);
			
			var raftCompletionChange:int = gameState.raftCompletionPercentage - previousGameState.raftCompletionPercentage;
			updateTotalText(raftCompletionTotalText, gameState.raftCompletionPercentage, "%");
			updateChangeText(raftCompletionChangeText, raftCompletionChange, "%");
			
			var sentenceNumber:int = Math.random() * DidYouKnowSentences.sentences.length;
			didYouKnowText.text = DidYouKnowSentences.sentences[sentenceNumber];
			
			// Arrange texts and labels according to which ones are visible
			var textY:int = 100;
			for (var i:int = 0; i < changeTexts.length; i++) {
				if (changeTexts[i].visible) {
					labels[i].visible = true;
					totalTexts[i].visible = true;
					
					labels[i].y = textY + this.y;
					totalTexts[i].y = textY;
					changeTexts[i].y = textY;
					textY += 30;
				} else {
					labels[i].visible = false;
					totalTexts[i].visible = false;
				}
			}


			previousGameState = ObjectUtil.copy(gameState) as GameState;
		}
		
		private function updateChangeText(changeText:Text, changeValue:int, suffix:String=""):void {
			changeText.text = changeValue.toString();
			if (changeValue > 0) {
				changeText.text = "+" + changeText.text;
			}
			changeText.text += suffix;
			
			changeText.visible = true;
			
			if (changeValue > 0) {
				changeText.color = 0x00FF00;
			} else if (changeValue < 0) {
				changeText.color = 0xFF0000;
			} else {
				changeText.visible = false;
			}
		}
		
		private function updateTotalText(text:Text, value:Number, suffix:String = ""):void {
			text.text = value + suffix;			
		}
		
		override public function show():void 
		{
			super.show();
			refresh();
		}
	}

}