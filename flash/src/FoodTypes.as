package 
{
	public class FoodTypes
	{
		public static var airlineMeal:Food = new Food("Airline Meal", 5);
		public static var apple:Food = new Food("Apple", 10);	
		public static var banana:Food = new Food("Banana", 20);	
		public static var fish:Food = new Food("Fish", 2);	
		public static var nothing:Food = new Food("Nothing", 0);	
		
		public static var list:Array = [airlineMeal, apple, banana, fish, nothing];
	}
	
}