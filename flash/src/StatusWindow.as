package 
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import punk.ui.PunkText;
	import punk.ui.PunkWindow;
	import punk.ui.PunkButton;
	import net.flashpunk.FP;
	import punk.ui.PunkLabel;

	
	public class StatusWindow extends Window
	{
		private var gameState:GameState;
		
		private var waterRemainingLabel:PunkLabel;
		private var airlineMealsRemainingLabel:PunkLabel;
		private var applesRemainingLabel:PunkLabel;
		private var bananasRemainingLabel:PunkLabel;
		private var fishRemainingLabel:PunkLabel;
		private var insulinRemainingLabel:PunkLabel;
		private var raftCompletionLabel:PunkLabel;
		
		private var waterJobLabel:PunkLabel;
		private var fruitJobLabel:PunkLabel;
		private var fishJobLabel:PunkLabel;
		private var guardJobLabel:PunkLabel;
		
		private var turnsPlayedLabel:PunkLabel;
				
		private var numPeopleWaterJob:int;
		private var numPeopleFruitJob:int;
		private var numPeopleFishJob:int;
		private var numPeopleGuardJob:int;
		private var numPeopleNoJob:int;
		
		public function StatusWindow(gameState:GameState) {
			super(200, 80, 400, 400, "Status");
			this.gameState = gameState;
		}
		
		override public function added():void {
			add(raftCompletionLabel = new PunkLabel("", 5, 30, 200, 25));
			add(airlineMealsRemainingLabel = new PunkLabel("", 5, 60, 200, 25));
			add(applesRemainingLabel = new PunkLabel("", 5, 90, 200, 25));
			add(bananasRemainingLabel = new PunkLabel("", 5, 120, 200, 25));
			add(fishRemainingLabel = new PunkLabel("", 5, 150, 200, 25));
			add(insulinRemainingLabel = new PunkLabel("", 5, 180, 200, 25));
			add(waterRemainingLabel = new PunkLabel("", 5, 210, 200, 25));
			
			add(waterJobLabel = new PunkLabel("", 5, 240, 200, 25));
			add(fruitJobLabel = new PunkLabel("", 5, 270, 200, 25));
			add(fishJobLabel = new PunkLabel("", 5, 300, 200, 25));
			add(guardJobLabel = new PunkLabel("", 5, 330, 200, 25));
			
			
			add(turnsPlayedLabel = new PunkLabel("", 5, 360, 200, 25));
			
			refresh();
			
		}
		
		public function refresh():void {
			waterRemainingLabel.text = "Water Remaining: " + String(gameState.waterAmount);
			airlineMealsRemainingLabel.text = "Airline Meals Remaining: " + String(gameState.airlineMealAmount);
			applesRemainingLabel.text = "Apples Remaining: " + String(gameState.appleAmount);
			bananasRemainingLabel.text = "Bananas Remaining: " + String(gameState.bananaAmount);
			fishRemainingLabel.text = "Fish Remaining: " + String(gameState.fishAmount);
			insulinRemainingLabel.text = "Insulin Units Remaining: " + String(gameState.insulinAmount); 
			raftCompletionLabel.text = "Raft completion: " + String(gameState.raftCompletionPercentage) + "%";
			
			numPeopleWaterJob = 0;
			numPeopleFruitJob = 0;
			numPeopleFishJob = 0;
			numPeopleGuardJob = 0;
			numPeopleNoJob = 0;
			for each(var person:Person in gameState.personList) {
				if (person.job == JobTypes.waterCollecting) {
					numPeopleWaterJob++;
				}
				else if (person.job == JobTypes.fishing) {
					numPeopleFishJob++;
				}
				else if (person.job == JobTypes.fruitGathering) {
					numPeopleFruitJob++;
				}
				else if (person.job == JobTypes.guarding) {
					numPeopleGuardJob++;
				}
				else if (person.job == JobTypes.idle) {
					numPeopleNoJob++;
				}
			}
			waterJobLabel.text = "Water Gatherers: " + String(numPeopleWaterJob); 
			fruitJobLabel.text = "Fruit Gatherers: " + String(numPeopleFruitJob); 
			fishJobLabel.text = "People Fishing: " + String(numPeopleFishJob); 
			guardJobLabel.text = "Number of Guards: " + String(numPeopleGuardJob); 
						
			turnsPlayedLabel.text = "Days Passed: " + String(gameState.turnsPlayed/2); 

		}
		
		override public function show():void 
		{
			super.show();
			refresh();
		}
	}

}