package
{
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import punk.ui.PunkPanel;
	import punk.ui.PunkText;
	
	import punk.ui.PunkButton;
	import punk.ui.PunkLabel;
	import punk.ui.PunkPasswordField;
	import punk.ui.PunkRadioButton;
	import punk.ui.PunkRadioButtonGroup;
	import punk.ui.PunkTextArea;
	import punk.ui.PunkTextField;
	import punk.ui.PunkToggleButton;
	import punk.ui.PunkUI;
	import punk.ui.PunkWindow;
	import punk.ui.skins.YellowAfterlife;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	import net.flashpunk.Sfx;
	
	public class EndGameWorld extends World
	{
		private var backgroundImage: Image;
		
		private var nextButton:PunkButton;
		
		private var infoWindow:PunkWindow;
		
		//public static var music: Sfx = new Sfx(Resources.DEFAULT_MUSIC);

		public function EndGameWorld()
		{
			super();
		}
		
		override public function begin():void
		{
			backgroundImage = new Image(Resources.DEFAULT_BACKGROUND);
			backgroundImage.x = -50;
			backgroundImage.y = 0;
			addGraphic(backgroundImage);
						
			//if (!music.playing) {
				//music.loop();
			//}
			
			infoWindow = new PunkWindow(200, 80, 400, 400, "Victory", false);
			infoWindow.add(new PunkButton(10, 340, 380, 50, "Back to Menu", returnToMenu));
			Text.size = 18;
			var endText:Text = new Text(
			"\t\t     Congratulations!\n\n\t\tYou escaped the island!",
			10, 60, 380, 340);
			endText.color = 0x000000;
			infoWindow.addGraphic(endText);
			
			add(infoWindow);

		}
		
		override public function end():void {
			// We need to remove all our Entities otherwise they will continue to exist
			removeAll();
		}
		public function returnToMenu():void {
			FP.world = new TitleWorld;
		}
	}
}