package {
	import net.flashpunk.graphics.Image;
	import mx.core.BitmapAsset;

	public class Job {
		public var name:String;
		public var description:String
		public var imageSource:Class = Resources.DEFAULT_PORTRAIT;
		
		public function Job(name:String="", description:String="", imageSource:Class=null) {
			this.name = name;
			this.description = description;
			this.imageSource = imageSource;
		}
	}

}