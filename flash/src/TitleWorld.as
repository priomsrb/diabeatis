package
{
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.fscommand;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import punk.ui.PunkPanel;
	import punk.ui.PunkText;
	
	import punk.ui.PunkButton;
	import punk.ui.PunkLabel;
	import punk.ui.PunkPasswordField;
	import punk.ui.PunkRadioButton;
	import punk.ui.PunkRadioButtonGroup;
	import punk.ui.PunkTextArea;
	import punk.ui.PunkTextField;
	import punk.ui.PunkToggleButton;
	import punk.ui.PunkUI;
	import punk.ui.PunkWindow;
	import punk.ui.skins.YellowAfterlife;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	import net.flashpunk.Sfx;
	
	public class TitleWorld extends World
	{
		private var backgroundImage: Image;
		private var titleImage: Image;
		private var titleWindow:PunkWindow;
		private var optionsWindow:PunkWindow;
		private var introWindow:PunkWindow;
		
		private var newGameButton:PunkButton;
		private var loadGameButton:PunkButton;
		private var optionsButton:PunkButton;
		private var moreInfoButton:PunkButton;
		
		private var muteButton:PunkButton;
		private var loadedGameState:GameState = new GameState;
		
		public static var music: Sfx = new Sfx(Resources.DEFAULT_MUSIC);
		
		private var fruitImage:Image;
		private var fishImage:Image;
		private var planeImage:Image;
		private var raftImage:Image;
		private var waterImage:Image;
		private var guardImage:Image;
		private var insulinImage:Image;
		
		private var introText:Text;
		private var continueButtonPressedCount:int = 0;

		public function TitleWorld()
		{
			super();
		}
		
		override public function begin():void
		{
			backgroundImage = new Image(Resources.DEFAULT_BACKGROUND);
			backgroundImage.x = -50;
			backgroundImage.y = 0;
			addGraphic(backgroundImage);
			
			titleImage = new Image(Resources.TITLE);
			titleImage.x = 400 - titleImage.width / 2;
			titleImage.y = 40;
			addGraphic(titleImage);
			
			//public function PunkWindow(x:Number=0, y:Number=0, width:int=20, height:int=20, caption:String = "", draggable:Boolean = true, skin:PunkSkin=null)	
						
			titleWindow = new PunkWindow(200, 150, 400, 400, "Menu", false);
			titleWindow.add(newGameButton = new PunkButton(10, 40, 380, 50, "New Game", newGameButtonPressed));
			titleWindow.add(loadGameButton = new PunkButton(10, 100, 380, 50, "Load Game", loadGameButtonPressed));
			titleWindow.add(optionsButton = new PunkButton(10, 160, 380, 50, "Options", optionsButtonPressed));
			titleWindow.add(moreInfoButton = new PunkButton(10, 220, 380, 50, "More About Diabetes", moreInfo));
			titleWindow.add(moreInfoButton = new PunkButton(10, 340, 380, 50, "Quit", closeGame));
			add(titleWindow);
			
			optionsWindow = new PunkWindow(200, 150, 400, 400, "Options", false);
			optionsWindow.add(muteButton = new PunkButton(10, 40, 380, 50, "Mute", toggleMute));
			optionsWindow.add(new PunkButton(10, 100, 380, 50, "Save"));
			optionsWindow.add(new PunkButton(10, 340, 380, 50, "Back", backFromOptions));
			
			introWindow = new PunkWindow(10, 10, 780, 580, "Introduction", false);
			planeImage = new Image(Resources.DEFAULT_PLANE);
			planeImage.x = 10;
			planeImage.y = 50;
			introWindow.addGraphic(planeImage);
			introWindow.add(new PunkLabel("Welcome to Diabeatis, a turn based game about diabetes!!!", 100, 45, 300, 300));
			introWindow.add(new PunkLabel("You are on a plane that has just crashed on an island with a bunch of", 100, 63, 300, 300));
			introWindow.add(new PunkLabel("your friends, and all of you have diabetes.", 100, 81, 300, 300));
			
			raftImage = new Image(Resources.RAFT_JOB);
			raftImage.x = 670;
			raftImage.y = 60;
			introWindow.addGraphic(raftImage);
			introWindow.add(new PunkLabel("Your goal is to escape from the island while managing your condition.", 80, 110, 300, 300));
		
			fruitImage = new Image(Resources.FRUITS_JOB);
			fruitImage.x = 115;
			fruitImage.y = 162;
			fruitImage.scale = 0.35;
			introWindow.addGraphic(fruitImage);
			
			fishImage = new Image(Resources.FISHING_JOB);
			fishImage.x = 155;
			fishImage.y = 185;
			fishImage.scale = 0.35;
			introWindow.addGraphic(fishImage);
			
			guardImage = new Image(Resources.GUARDING_JOB);
			guardImage.x = 290;
			guardImage.y = 352;
			guardImage.scale = 0.33;
			introWindow.addGraphic(guardImage);
			
			waterImage = new Image(Resources.WATER_JOB);
			waterImage.x = 188;
			waterImage.y = 328;
			waterImage.scale = 0.35;
			introWindow.addGraphic(waterImage);
			
			insulinImage = new Image(Resources.DEFAULT_NEEDLE);
			insulinImage.x = 305;
			insulinImage.y = 212;
			insulinImage.scale = 0.45;
			introWindow.addGraphic(insulinImage);
			
			Text.size = 16;
			introText = new Text("To do this you must manage the diet of the people on the island from the 'Health' menu\n"
			+"Eating fruit	     for example will significantly increase your blood sugar levels,\n"
			+"while eating fish	    or airline food will have a smaller effect. Not eating will cause\n"
			+"blood sugar to drop slightly. Insulin		can be used to significantly reduce blood sugar\n"
			+"levels, but you have a limited supply.\n"
			+"You will also have to obtain more resources to feed people by assigning people to Jobs.\n"
			+"People who are not fed will not be able to complete a job, as will people who have very high\n"
			+"or very low blood sugar levels\n"
			+"Running out of water	   will also cause jobs to fail until more water is gathered.\n"
			+"It will also be important to Guard	your camp from monkeys, who can steal your resources\n"
			+"Your ultimate goal is to build a raft to escape \n",
			20, 145, 760, 440);
			introText.color = 0x000000;
			introWindow.addGraphic(introText);
			
			introWindow.add(new PunkButton(390, 520, 190, 50, "Back", backFromIntro));
			introWindow.add(new PunkButton(580, 520, 190, 50, "Continue", next));
						
			// If there is no saved game then disable the load button
			var gameStateSharedObject:SharedObject = SharedObject.getLocal("gameState");
			if (gameStateSharedObject.size != 0) {
				loadedGameState = gameStateSharedObject.data.gameState;	
			} else {
				loadGameButton.active = false;
			}
			
			Options.load();
			updateMuteButton();
			
			if (!music.playing) {
				music.loop();
			}
			
		}
		
		override public function end():void {
			// We need to remove all our Entities otherwise they will continue to exist
			removeAll();
		}
		
		public function newGameButtonPressed():void {
			remove(titleWindow);
			add(introWindow);
			
		}
		public function loadGameButtonPressed() : void {		
			FP.world = new GameWorld(loadedGameState);
		}
		public function optionsButtonPressed() : void {
			remove(titleWindow);
			add(optionsWindow);
		}
		
		private function toggleMute(): void {
			Options.mute = !Options.mute;
			updateMuteButton();
		}
		
		private function updateMuteButton():void {
			if (Options.mute == true) {
				muteButton.label.text = "Unmute";
			} else {
				muteButton.label.text = "Mute";
			}
		}
		
		public function moreInfo() : void {
			navigateToURL(new URLRequest("http://en.wikipedia.org/wiki/Diabetes_mellitus"));
			//HTML link to external site with more info on diabetes
		}
		public function closeGame() : void {
			fscommand("quit");
		}
		public function saveGame() : void {
			//Some way of saving game
		}
		public function next():void {
			if (continueButtonPressedCount == 0){
			introText.text = "About Diabetes:\nType II Diabetes is a chronic metabolic condition which results in high blood sugar\n"
			+"levels. It occurs when the body doesn't produce enough insulin (the hormone in the\n"
			+"body that regulates the uptake of glucose), or cells in the body develop an insulin\n"
			+"resistance. Management of the condition involves keeping blood sugar levels at an\n"
			+"acceptable level through good lifestyle choices, a healthy diet and exercise to\n"
			+"maintain a healthy weight";
			introWindow.removeGraphic(fruitImage);
			introWindow.removeGraphic(fishImage);
			introWindow.removeGraphic(guardImage);
			introWindow.removeGraphic(waterImage);
			introWindow.removeGraphic(insulinImage);
			continueButtonPressedCount++;
			}
			else {
				FP.world = new GameWorld(new GameState);
				continueButtonPressedCount = 0;
			}
		}
		public function backFromIntro():void {
			remove(introWindow);
			add(titleWindow);
		}
		public function backFromOptions():void {
			remove(optionsWindow);
			add(titleWindow);
		}
	}
}