package
{
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Text;
	import flash.external.ExternalInterface;
	import flash.text.TextField;
    import flash.utils.Timer;
    import flash.text.TextFieldType;
    import flash.text.TextFieldAutoSize;
	import flash.events.*;
	import punk.ui.PunkButton;
	
	public class GameWorld extends World
	{			
		private var backgroundImage: Image;
		private var sunImage: Image;
		
		private var healthWindow:HealthWindow;
		private var jobsWindow:JobsWindow;
		private var optionsWindow:OptionsWindow;
		private var statusWindow:StatusWindow;
		private var summaryWindow:SummaryWindow;
		private var monkeyAttackWindow:MonkeyAttackWindow;
		private var timeOfDayOverlay:TimeOfDayOverlay;
		
		private var nextButton:PunkButton;
		
		private var gameState:GameState;
						
		public function GameWorld(loadedGameState:GameState) {
			gameState = loadedGameState;
		}		
		
		override public function begin():void {
			backgroundImage = new Image(Resources.DEFAULT_BACKGROUND);
			backgroundImage.x = -50;
			backgroundImage.y = 0;
			addGraphic(backgroundImage);
			
			add(timeOfDayOverlay = new TimeOfDayOverlay(gameState, newTurnStarted));
			
			
			add(healthWindow = new HealthWindow(gameState));
			add(jobsWindow = new JobsWindow(gameState));
			add(optionsWindow = new OptionsWindow(gameState));
			add(statusWindow = new StatusWindow(gameState));
			add(summaryWindow = new SummaryWindow(gameState));
			add(monkeyAttackWindow = new MonkeyAttackWindow(gameState));
			
			hideAllWindows();
			monkeyAttackWindow.onHideCallback = summaryWindow.show;

			add(new PunkButton(10, 500, 148, 90, "Health", healthButtonPressed));
			add(new PunkButton(168, 500, 148, 90, "Jobs", jobsButtonPressed));
			add(new PunkButton(326, 500, 148, 90, "Status", statusButtonPressed));
			add(new PunkButton(484, 500, 148, 90, "Options", optionsButtonPressed));
			add(nextButton = new PunkButton(642, 500, 148, 90, "Next", nextButtonPressed));
		}
		
		override public function end():void {
			// We need to remove all our Entities otherwise they will continue to exist
			removeAll();
		}
		
		private function nextButtonPressed() : void {
			hideAllWindows();
			timeOfDayOverlay.progressDay();
			
			gameState.doTurn();
			
			if (gameState.raftCompletionPercentage>=100) {
				FP.world = new EndGameWorld;
			}
			
		}
		
		private function newTurnStarted() : void {
			hideAllWindows();
			
			if (gameState.monkeysAttacked) {
				monkeyAttackWindow.show();
			} else {
				summaryWindow.show();
			}

		}
		
		override public function update():void 
		{
			super.update();
			if (timeOfDayOverlay.timeOfDay != TimeOfDayOverlay.MORNING &&
			timeOfDayOverlay.timeOfDay != TimeOfDayOverlay.AFTERNOON) {
				nextButton.active = false;
			} else {
				nextButton.active = true;
			}
			
		}
		
		private function healthButtonPressed() : void {
			toggleWindow(healthWindow);
		}
		
		private function jobsButtonPressed() : void {
			toggleWindow(jobsWindow);
		}
		private function optionsButtonPressed() : void {
			toggleWindow(optionsWindow);
		}
		
		private function statusButtonPressed() : void {
			statusWindow.refresh();
			toggleWindow(statusWindow);
		}
		
		private function toggleWindow(window:Window) : void {
			if (window.visible) {
				hideAllWindows();
			} else {
				hideAllWindows();
				window.show();
			}
		}

		private function hideAllWindows() : void {
			healthWindow.hide();
			jobsWindow.hide();
			optionsWindow.hide();
			statusWindow.hide();
			monkeyAttackWindow.hide();
			summaryWindow.hide();
		}
		
		
	}

}