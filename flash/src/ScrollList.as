package 
{
	import net.flashpunk.Entity;
	import punk.ui.PunkPanel;
	import punk.ui.PunkUIComponent;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input
	
	public class ScrollList extends PunkUIComponent {
		private var scrollBar : ScrollBar;
		protected var children:Vector.<PunkUIComponent> = new Vector.<PunkUIComponent>;
		protected var totalHeight: int = 0;
		protected var panel:PunkPanel;
		public var padding:int = 5;
		//protected var x, y, width, height;
		
		public function ScrollList(x:int, y:int, width:int, height:int, panel:PunkPanel) {
			super(x, y, width, height);
			
			this.panel = panel;
			
			//this.x = x + panel.x;
			//this.y = y + panel.y;
			//this.width = width;
			//this.height = height;
			

		}
		
		public override function added():void {
			scrollBar = new ScrollBar(x + width - 30 - panel.x, y - panel.y, 30, height, refresh);
			panel.add(scrollBar);
		}
		
		public function add(uiComponent : PunkUIComponent) : void {
			var position:int = children.length;
			children[position] = uiComponent;
			uiComponent.y = totalHeight;
			totalHeight += uiComponent.height + padding;
			
			uiComponent.x = x - panel.x;
			panel.add(uiComponent);
			
			if (scrollBar) {
				var buttonHeight:int = height / (totalHeight - padding) * height;
				buttonHeight = Math.max(90, Math.min(height, buttonHeight));
				scrollBar.buttonHeight = buttonHeight;
			}
			
			refresh();
		}
		
		override public function update():void {
			if (collidePoint(x, y, FP.world.mouseX, FP.world.mouseY)) {
				if (Input.mouseWheel) {
					var progressPerItem:Number = Math.max(0, (totalHeight - height)) / (children[0].height + padding);
					scrollBar.scrollProgress -= Input.mouseWheelDelta / 3 / progressPerItem;
					refresh();
				}
			}
			
		}
		
		private function refresh():void {
			var scrollPosition:int = (totalHeight - height) * scrollBar.scrollProgress;
			scrollPosition = Math.max(0, scrollPosition);
			scrollPosition = Math.round(scrollPosition / (children[0].height + padding)) * (children[0].height + padding);
			var nextY:int = y - scrollPosition;
			
			
			for each(var uiComponent:PunkUIComponent in children) {
				uiComponent.y = nextY;
				nextY += uiComponent.height + padding;
				
				if (uiComponent.y < y || uiComponent.y + uiComponent.height > y + height) {
					uiComponent.visible = false;
					uiComponent.active = false;
				} else {
					uiComponent.visible = true;
					uiComponent.active = true;
				}
				

			}
		}
	}
}