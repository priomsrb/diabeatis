package {
	import net.flashpunk.FP;
	public class PersonGenerator {
		
		public static function generatePerson() : Person {
			var firstNames:Array = new Array(
				"James",
				"John",
				"Robert",
				"Michael",
				"William",
				"David",
				"Richard",
				"Charles",
				"Joseph",
				"Thomas",
				"Christopher",
				"Daniel",
				"Paul",
				"Mark",
				"Donald",
				"George",
				"Kenneth",
				"Steven",
				"Edward",
				"Brian",
				"Patricia",
				"Linda",
				"Barbara",
				"Elizabeth",
				"Jennifer",
				"Maria",
				"Susan",
				"Margaret",
				"Dorothy",
				"Lisa",
				"Nancy",
				"Karen",
				"Betty",
				"Helen",
				"Sandra",
				"Donna",
				"Carol",
				"Ruth",
				"Sharon",
				"Michelle"
			);
			
			var lastNames:Array = new Array(
				"Smith",
				"Johnson",
				"Williams",
				"Brown",
				"Jones",
				"Miller",
				"Davis",
				"Garcia",
				"Rodriguez",
				"Wilson",
				"Martinez",
				"Anderson",
				"Taylor",
				"Thomas",
				"Hernandez",
				"Moore",
				"Martin",
				"Jackson",
				"Thompson",
				"White",
				"Lopez",
				"Lee",
				"Gonzalez",
				"Harris",
				"Clark",
				"Lewis",
				"Robinson",
				"Walker",
				"Perez",
				"Hall",
				"Young",
				"Allen",
				"Sanchez",
				"Wright",
				"King",
				"Scott",
				"Green",
				"Baker",
				"Adams",
				"Nelson"
			);
			
			var firstName:String = firstNames[Math.floor(Math.random() * firstNames.length)];
			var lastName:String = lastNames[Math.floor(Math.random() * lastNames.length)];
			
			var randomBloodSugar:Number = 35 + Math.random()*30;
			
			var person:Person = new Person(firstName + " " + lastName);
			person.bloodSugar = randomBloodSugar;
			person.imageTint = FP.getColorHSV(Math.random(), 1, 1);
			
			return person;
		}
	}
	
}